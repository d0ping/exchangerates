//
//  DataManager.m
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import "DataManager.h"

NSString * const kDataManagerErrorDomain = @"DataManagerErrorDomain";
NSString * const kCurrentExchangeTypeDidUpdateNotification = @"CurrentExchangeTypeDidUpdateNotification";

static NSString * const kAPIKey     = @"jr-6836fc9b7e8956db90a1d47a9abed207";
static NSString * const kBaseURL    = @"http://jsonrates.com/historical/?";

@implementation DataManager

#pragma mark - singleton

+ (instancetype)defaultManager {
    static dispatch_once_t  onceToken;
    static id sSharedInstance = nil;
    
    dispatch_once(&onceToken, ^{
        sSharedInstance = [[self alloc] init];
    });
    return sSharedInstance;
}

#pragma mark - Helpers

- (NSString *)firstCurrencyCodeFromExchangeType:(ERExchangeType)type {
    switch (type) {
        case ERExchangeTypeEURRUB:
        case ERExchangeTypeEURUSD:
            return @"EUR";
            break;
        case ERExchangeTypeRUBEUR:
        case ERExchangeTypeRUBUSD:
            return @"RUB";
            break;
        case ERExchangeTypeUSDEUR:
        case ERExchangeTypeUSDRUB:
        default:
            return @"USD";
            break;
    }
}

- (NSString *)secondCurrencyCodeFromExchangeType:(ERExchangeType)type {
    switch (type) {
        case ERExchangeTypeRUBEUR:
        case ERExchangeTypeUSDEUR:
            return @"EUR";
            break;
        case ERExchangeTypeEURUSD:
        case ERExchangeTypeRUBUSD:
            return @"USD";
            break;
        case ERExchangeTypeEURRUB:
        case ERExchangeTypeUSDRUB:
        default:
            return @"RUB";
            break;
    }
}

#pragma mark - Setters

- (void)setCurrentExchangeType:(ERExchangeType)currentExchangeType {
    if (_currentExchangeType != currentExchangeType) {
        _currentExchangeType = currentExchangeType;
        [[NSNotificationCenter defaultCenter] postNotificationName:kCurrentExchangeTypeDidUpdateNotification object:nil];
    }
}

#pragma mark - Public

- (NSString *)currencyDescriptionFromExchangeType:(ERExchangeType)type {
    switch (type) {
        case ERExchangeTypeEURUSD:
        case ERExchangeTypeEURRUB:
            return NSLocalizedString(@"euro", @"euro");
            break;
        case ERExchangeTypeRUBUSD:
        case ERExchangeTypeRUBEUR:
            return NSLocalizedString(@"ruble", @"russian ruble");
            break;
        case ERExchangeTypeUSDRUB:
        case ERExchangeTypeUSDEUR:
        default:
            return NSLocalizedString(@"dollar", @"us dollar");
            break;
    }
}

- (NSString *)exchangeTypeDescriptionForType:(ERExchangeType)type {
    NSString *fromCurrecyCode = [self firstCurrencyCodeFromExchangeType:type];
    NSString *toCurrencyCode = [self secondCurrencyCodeFromExchangeType:type];
    return [NSString stringWithFormat:@"%@ ➔ %@", fromCurrecyCode, toCurrencyCode];
}

- (void)loadExchangeRateType:(ERExchangeType)type
                startDateStr:(NSString *)startDateStr
                  endDateStr:(NSString *)endDateStr
                   onSuccess:(void(^)(NSDictionary *json))success
                   onFailure:(void(^)(NSError *error))failure {
    
    NSString *fromCurrecyCode = [self firstCurrencyCodeFromExchangeType:type];
    NSString *toCurrencyCode = [self secondCurrencyCodeFromExchangeType:type];
    
    NSString *requesteURLString = [kBaseURL stringByAppendingFormat:@"from=%@&to=%@&dateStart=%@&dateEnd=%@&apiKey=%@", fromCurrecyCode, toCurrencyCode, startDateStr, endDateStr, kAPIKey];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:requesteURLString]];
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSError* error;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if (nil == json[@"error"]) {
                if (success) {
                    success(json);
                }
            } else {
                error = [[NSError alloc] initWithDomain:kDataManagerErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey: json[@"error"]}];
                if (failure) {
                    failure(error);
                }
            }
        });
        
    });
}

@end
