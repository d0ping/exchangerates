//
//  UIColor+ERLibrary.m
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import "UIColor+ERLibrary.h"

@implementation UIColor (ERLibrary)

#pragma mark - Helpers

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

#pragma mark - Public

+ (UIColor *)erGrayColor {
    return [UIColor colorFromHexString:@"#3F4753"];
}

+ (UIColor *)erGreenColor {
    return [UIColor colorFromHexString:@"#7ED321"];
}

+ (UIColor *)erRedColor {
    return [UIColor colorFromHexString:@"#CE0D24"];
}

@end
