//
//  UIFont+ERLibrary.m
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import "UIFont+ERLibrary.h"

@implementation UIFont (ERLibrary)

+ (UIFont *)fontLatoBlackWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Lato-Black" size:size];
}

+ (UIFont *)fontLatoBoldWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Lato-Bold" size:size];
}

+ (UIFont *)fontLatoMediumItalicWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Lato-MediumItalic" size:size];
}

+ (UIFont *)fontLatoRegularWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Lato-Regular" size:size];
}

@end
