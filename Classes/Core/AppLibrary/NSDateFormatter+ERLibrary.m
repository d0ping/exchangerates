//
//  NSDateFormatter+ERLibrary.m
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import "NSDateFormatter+ERLibrary.h"

@implementation NSDateFormatter (ERLibrary)

+ (NSDateFormatter *)formatterAPIRequest {
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    });
    return dateFormatter;
}

+ (NSDateFormatter *)formatterDefaultTime {
    static dispatch_once_t formatterInit;
    static NSDateFormatter *formatter;
    dispatch_once(&formatterInit, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm"];
    });
    return formatter;
}

@end
