//
//  NSDateFormatter+ERLibrary.h
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (ERLibrary)

+ (NSDateFormatter *)formatterAPIRequest;
+ (NSDateFormatter *)formatterDefaultTime;

@end
