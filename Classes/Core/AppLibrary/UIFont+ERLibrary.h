//
//  UIFont+ERLibrary.h
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (ERLibrary)

+ (UIFont *)fontLatoBlackWithSize:(CGFloat)size;
+ (UIFont *)fontLatoBoldWithSize:(CGFloat)size;
+ (UIFont *)fontLatoMediumItalicWithSize:(CGFloat)size;
+ (UIFont *)fontLatoRegularWithSize:(CGFloat)size;

@end
