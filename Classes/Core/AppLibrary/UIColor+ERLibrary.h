//
//  UIColor+ERLibrary.h
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ERLibrary)

+ (UIColor *)erGrayColor;
+ (UIColor *)erGreenColor;
+ (UIColor *)erRedColor;

@end
