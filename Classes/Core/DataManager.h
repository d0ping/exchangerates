//
//  DataManager.h
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kCurrentExchangeTypeDidUpdateNotification;

typedef NS_ENUM(NSInteger, ERExchangeType) {
    ERExchangeTypeUSDRUB = 0,
    ERExchangeTypeUSDEUR,
    ERExchangeTypeEURRUB,
    ERExchangeTypeEURUSD,
    ERExchangeTypeRUBUSD,
    ERExchangeTypeRUBEUR,
};

@interface DataManager : NSObject

@property (nonatomic) ERExchangeType        currentExchangeType;
@property (nonatomic) NSDate                *currentRateTimestamp;
@property (nonatomic) float                 currentExchangeRate;
@property (nonatomic) float                 historyExchangeRate;

+ (instancetype)defaultManager;
- (NSString *)currencyDescriptionFromExchangeType:(ERExchangeType)type;
- (NSString *)exchangeTypeDescriptionForType:(ERExchangeType)type;
- (void)loadExchangeRateType:(ERExchangeType)type
                startDateStr:(NSString *)startDateStr
                  endDateStr:(NSString *)endDateStr
                   onSuccess:(void(^)(NSDictionary *json))success
                   onFailure:(void(^)(NSError *error))failure;

@end
