//
//  ViewController.h
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel                *exchangeTypeLabel;
@property (nonatomic, weak) IBOutlet UILabel                *rateLabel;
@property (nonatomic, weak) IBOutlet UILabel                *descriptionLabel;
@property (nonatomic, weak) IBOutlet UILabel                *lastUpdateLabel;
@property (nonatomic, weak) IBOutlet UIButton               *menuButton;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint     *rateLabelTopOffsetConstraint;

@end

