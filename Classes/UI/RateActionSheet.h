//
//  RateActionSheet.h
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RateActionSheet;
@protocol RateActionSheetDelegate <NSObject>

@required
- (void)rateActionSheet:(RateActionSheet *)actionSheet didSelectExchangeType:(ERExchangeType)type;

@end

@interface RateActionSheet : UIView

@property (nonatomic, weak) id<RateActionSheetDelegate>     delegate;
@property (nonatomic, readonly, getter=isShown) BOOL        show;

- (void)showRouteActionSheetInView:(UIView *)view;
- (void)dismissRouteActionSheet;

@end
