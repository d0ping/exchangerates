//
//  RateActionSheet.m
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import "RateActionSheet.h"

const CGFloat kRateActionSheetBtnHeight     = 43.f;
const CGFloat kRateActionSheetBtnOffset     = .5f;

@interface RateActionSheet ()

@property (nonatomic, strong) UIView            *primaryView;
@property (nonatomic, strong) NSMutableArray    *rateButtonArray;

@end

@implementation RateActionSheet

- (instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor blackColor];
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//        self.clipsToBounds = YES;
        
        _rateButtonArray = [[NSMutableArray alloc] initWithCapacity:6];
        
        for (int i=ERExchangeTypeUSDRUB; i<=ERExchangeTypeRUBEUR; i++) {
            UIButton *rateButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [rateButton setTitle:[[DataManager defaultManager] exchangeTypeDescriptionForType:i] forState:UIControlStateNormal];
            rateButton.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            rateButton.tag = i;
            rateButton.backgroundColor = [UIColor erGrayColor];
            [rateButton setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:.7f] forState:UIControlStateNormal];
            [rateButton.titleLabel setFont:[UIFont fontLatoRegularWithSize:14.f]];
            [self addSubview:rateButton];
            [_rateButtonArray addObject:rateButton];
            [rateButton addTarget:self action:@selector(rateButtonDidSelect:) forControlEvents:UIControlEventTouchUpInside];

        }
        [self updateButtonWithCurrentExchangeType];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(currentExchangeTypeDidUpdateNotification:) name:kCurrentExchangeTypeDidUpdateNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - CurrentExchangeType

- (void)currentExchangeTypeDidUpdateNotification:(NSNotification *)notification {
    [self updateButtonWithCurrentExchangeType];
}

- (void)updateButtonWithCurrentExchangeType {
    ERExchangeType currentExchangeType = [DataManager defaultManager].currentExchangeType;
    for (UIButton *rateBtn in _rateButtonArray) {
        BOOL isCur = ( rateBtn.tag == currentExchangeType );
        [rateBtn setTitleColor: ( isCur ? [UIColor whiteColor] : [[UIColor whiteColor] colorWithAlphaComponent:.7f] ) forState:UIControlStateNormal];
        [rateBtn.titleLabel setFont:( isCur ? [UIFont fontLatoBlackWithSize:14.f] : [UIFont fontLatoRegularWithSize:14.f] )];
    }
}

#pragma mark - Actions

- (void)rateButtonDidSelect:(UIButton *)button {
    if ([_delegate respondsToSelector:@selector(rateActionSheet:didSelectExchangeType:)]) {
        [_delegate rateActionSheet:self didSelectExchangeType:button.tag];
    }
}

#pragma mark - Drawing

- (void)drawSheetOnView:(UIView *)view show:(BOOL)show  {
    CGFloat btnPanelHeight = kRateActionSheetBtnOffset *(_rateButtonArray.count +1) + kRateActionSheetBtnHeight *_rateButtonArray.count;
    CGFloat btnPanelY = CGRectGetHeight(view.bounds) - ( show ? btnPanelHeight : .0f );
    self.frame = CGRectMake(.0f, btnPanelY, view.bounds.size.width, btnPanelHeight);
    
    CGSize btnSize = CGSizeMake(view.bounds.size.width - kRateActionSheetBtnOffset *2.f, kRateActionSheetBtnHeight);
    int btnIndex = 0;
    for (UIButton *rateBtn in _rateButtonArray) {
        CGFloat btnY = kRateActionSheetBtnOffset * (btnIndex +1) + kRateActionSheetBtnHeight * btnIndex;
        rateBtn.frame = CGRectMake(kRateActionSheetBtnOffset, btnY, btnSize.width, btnSize.height);
        btnIndex++;
    }
}

- (void)show:(BOOL)show withAnimated:(BOOL)animated {
    NSTimeInterval duration = .3f;

    __weak typeof(self)weakSelf = self;
    void (^beforeAnimationBlock)() = ^{
        if (show) {
            [weakSelf drawSheetOnView:weakSelf.primaryView show:NO];
            [_primaryView addSubview:weakSelf];
            _show = YES;
        }
    };
    void (^animationBlock)()  = ^{
        [weakSelf drawSheetOnView:weakSelf.primaryView show:show];
    };
    void (^completionBlock)(BOOL finished) = nil;
    if (NO == show) {
        completionBlock = ^(BOOL finished) {
            _show = NO;
            [weakSelf removeFromSuperview];
        };
    }
    
    if (animated) {
        beforeAnimationBlock();
        [UIView animateWithDuration:duration
                              delay:.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:animationBlock
                         completion:completionBlock];
    } else {
        animationBlock();
        if (completionBlock) {
            completionBlock(YES);
        }
    }
}

#pragma mark - Public

- (void)showRouteActionSheetInView:(UIView *)view {
    self.primaryView = view;
    [self show:YES withAnimated:YES];
}

- (void)dismissRouteActionSheet {
    [self show:NO withAnimated:YES];
    self.primaryView = nil;
}

@end
