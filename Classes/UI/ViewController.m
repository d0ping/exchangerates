//
//  ViewController.m
//  ExchangeRates
//
//  Created by Denis Bogatyrev on 13.05.15.
//  Copyright (c) 2015 DenisBogatyrev. All rights reserved.
//

#import "ViewController.h"
#import "RateActionSheet.h"

const NSTimeInterval kHistoricalTimeInterval    = 60*60*24;

@interface ViewController () <RateActionSheetDelegate>

@property (nonatomic, strong) RateActionSheet           *rateActionSheet;
    
@end

@implementation ViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    [self view];
    
    // Customization
    _exchangeTypeLabel.textColor = [UIColor erGrayColor];
    _rateLabel.textColor = [UIColor erGrayColor];
    _descriptionLabel.textColor = [UIColor erGreenColor];
    _lastUpdateLabel.textColor = [UIColor erGrayColor];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _rateActionSheet = [[RateActionSheet alloc] init];
    _rateActionSheet.delegate = self;
    [self.view addSubview:_rateActionSheet];
    
    [self updateCurrentExchangeType];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(currentExchangeTypeDidUpdateNotification:) name:kCurrentExchangeTypeDidUpdateNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - CurrentExchangeType

- (void)currentExchangeTypeDidUpdateNotification:(NSNotification *)notification {
    [self updateCurrentExchangeType];
}

- (void)updateCurrentExchangeType {
    ERExchangeType currentExchangeType = [DataManager defaultManager].currentExchangeType;
    _exchangeTypeLabel.text = [[DataManager defaultManager] exchangeTypeDescriptionForType:currentExchangeType];
    [self updateCurrentRateWithExchangeType:currentExchangeType];
}

- (void)updateCurrentRateWithExchangeType:(ERExchangeType)type {
    __weak typeof(self)weakSelf = self;
    
    NSString *startDateStr = [[NSDateFormatter formatterAPIRequest] stringFromDate:[NSDate dateWithTimeIntervalSinceNow:kHistoricalTimeInterval * -1]];
    NSString *endDateStr = [[NSDateFormatter formatterAPIRequest] stringFromDate:[NSDate date]];
    
    [[DataManager defaultManager] loadExchangeRateType:type
                                          startDateStr:startDateStr
                                            endDateStr:endDateStr
                                             onSuccess:^(NSDictionary *json) {
                                                 NSDictionary *rates = [json objectForKey:@"rates"];
                                                 
                                                 NSDictionary *currentRateDict = rates[endDateStr];
                                                 float rate = [[currentRateDict objectForKey:@"rate"] floatValue];
                                                 
                                                 NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                                                 [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                                                 [formatter setRoundingIncrement: [NSNumber numberWithDouble:0.001]];
                                                 NSString *rateStr = [formatter stringFromNumber:[NSNumber numberWithFloat:rate]];
                                                 
                                                 weakSelf.rateLabel.text = rateStr;
                                                 weakSelf.lastUpdateLabel.text = [[NSLocalizedString(@"Updated ", @"last update") stringByAppendingString:[[NSDateFormatter formatterDefaultTime] stringFromDate:[NSDate date]]] uppercaseString];
                                                 
                                                 NSDictionary *historicalRateDict = rates[startDateStr];
                                                 float historicalRate = [[historicalRateDict objectForKey:@"rate"] floatValue];
                                                 float rateDif = ( rate - historicalRate ) / historicalRate;
                                                 
                                                 weakSelf.descriptionLabel.textColor = ( rateDif >= 0 ? [UIColor erGreenColor] : [UIColor erRedColor] );
                                                 
                                                 NSString *currencyStr = [[DataManager defaultManager] currencyDescriptionFromExchangeType:type];
                                                 NSString *rateDifPercentsStr = [NSString stringWithFormat:@"%.0f %@", rateDif *100 *(rateDif < 0 ? -1 : 1), NSLocalizedString(@"percents", @"")];
                                                 weakSelf.descriptionLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@", NSLocalizedString(@"Since yesterday", @""), currencyStr, (rateDif > 0 ? NSLocalizedString(@"rose by", @"") : NSLocalizedString(@"fell by", @"") ), rateDifPercentsStr];
                                                 
                                             } onFailure:^(NSError *error) {
                                                 weakSelf.rateLabel.text = @"0";
                                                 [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                                             message:error.localizedDescription
                                                                            delegate:nil
                                                                   cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok button")
                                                                   otherButtonTitles:nil] show];
                                             }];
}

#pragma mark - Displaying RateActionSheet

- (void)displayRateActionSheet:(BOOL)display {
    NSTimeInterval duration = .3f;
    if (display) {
        [_rateActionSheet showRouteActionSheetInView:self.view];
    } else {
        [_rateActionSheet dismissRouteActionSheet];
    }
    
    __weak typeof(self)weakSelf = self;
    [self.view layoutIfNeeded];
    _rateLabelTopOffsetConstraint.constant = display ? 36.f : 100.f;
    
    void (^animationBlock)()  = ^{
        [weakSelf.view layoutIfNeeded];
    };
    
    [UIView animateWithDuration:duration
                          delay:.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:animationBlock
                     completion:nil];
}

#pragma mark - Actions

- (IBAction)menuButtonDidSelect:(UIButton *)button {
    [self displayRateActionSheet:YES];
}

#pragma mark - RateActionSheetDelegate

- (void)rateActionSheet:(RateActionSheet *)actionSheet didSelectExchangeType:(ERExchangeType)type {
    [DataManager defaultManager].currentExchangeType = type;
    [self displayRateActionSheet:NO];
}

@end
